/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.bitforce;

import java.util.Scanner;

/**
 * Interfaces with the Bank class to change account properties.
 * @author Clayton Fernandes
 */
public class AtmApplication {

    public AtmApplication() {

    }
    public static void main(String[] args) {
        AtmApplication atm = new AtmApplication();
        atm.run();
    }

    public void run() {
        Bank _bank = new Bank();
        Scanner input = new Scanner(System.in);
        int currentAccount;
        int currentFunction = -1;
        double currentBalance;
        boolean transactionSuccess;


        //Keeps looping until a valid account ID is entered.
        while(true) {
            try {
                System.out.println("Please enter your account ID: ");
                currentAccount = Integer.parseInt(input.nextLine());
                _bank.accountExists(currentAccount);
                break;
            }catch(IllegalArgumentException e) {
                System.out.println("Error: This account does not exist. Please try again.");
            }
        }

        currentBalance = updateBalance(_bank, currentAccount);

        //Keeps looping until a valid function numbere is entered.
        do{
            //reset vars
            currentFunction = -1;
            transactionSuccess = false;

            System.out.println("Please select a function:\n"
                    + "1 - Balance\n"
                    + "2 - Deposit\n"
                    + "3 - Withdraw\n"
                    + "0 - Exit");
            try {
                currentFunction = Integer.parseInt(input.nextLine());

                if(currentFunction < 0 || currentFunction > 4) {
                    System.out.println("Invalid function entered. Please try again.");
                }else {
                    //Get the user's input in a try-catch loop.
                    if(currentFunction == 1) { //Balance
                        System.out.println("Current balance: $" + doBalance(_bank, currentAccount));
                        transactionSuccess = true;

                    }else if(currentFunction == 2) { //Deposit
                        do{
                            System.out.println("Deposit\n"
                                    + "Please enter the amount: ");
                            try {
                                int depositAmount = Integer.parseInt(input.nextLine()); //Get deposit amount from user
                                transactionSuccess = doDeposit(_bank, currentAccount, depositAmount); //Complete deposit and set transactionSucess to value returned
                            }catch(IllegalArgumentException e) {
                                System.out.println("Invalid amount entered. Please try again.");
                            }
                        } while (!transactionSuccess);
                        currentBalance = updateBalance(_bank, currentAccount); //Update currentBalance after completing transaction.
                        System.out.println("Current Balance: $" + currentBalance);

                    }else if(currentFunction == 3) { //Withdraw
                        do{
                            System.out.println("Withdraw\n"
                                    + "Avaliable amount: " + currentBalance + "\n"
                                    + "Please enter the amount: ");
                            try { //Check if user had entered a valid number
                                int withdrawAmount = Integer.parseInt(input.nextLine()); //Get withdraw amount from user
                                transactionSuccess = doWithdraw(_bank, currentAccount, withdrawAmount);
                            }catch(IllegalArgumentException e) {
                                System.out.println("Invalid amount entered. Please try again.");
                            }
                        }while(!transactionSuccess);
                        currentBalance = updateBalance(_bank, currentAccount); //Update currentBalance after completing transaction.
                        System.out.println("Current Balance: $" + currentBalance);
                    }
                }
            }catch(IllegalArgumentException e) {
                System.out.println("Invalid function entered. Please try again.");
            }


            //If the last transaction was successful ask the user if they would like to do another
            if(transactionSuccess == true){
                System.out.println("Would you like to do another transaction?\n"
                        + "1 - No\n"
                        + "Enter - Yes");
                if(input.nextLine().equals("1")) {
                    currentFunction = 0;
                }
            }
        } while(currentFunction < 0 || currentFunction > 4 || currentFunction != 0);
    }

    /**
     *
     * @param accId
     * @param bank
     * @return the avaliable balance
     */
    public double updateBalance(Bank bank, int accId) {
        return bank.retrieveAccount(accId).getBalance();
    }

    /**
     *
     * @param bank bank
     * @param accId account Id
     * @param depositAmount deposit amount
     * @return true if the deposit transaction went though successfully
     */
    public boolean doDeposit(Bank bank, int accId, double depositAmount) {
        try {
            bank.retrieveAccount(accId).deposit(depositAmount); //Complete deposit
            System.out.println("Deposit complete.");
            return true;
        }catch(IllegalArgumentException e) {
            System.out.println("Invalid amount entered. Please try again.");
            return false;
        }
    }

    /**
     *
     * @param bank bank
     * @param accId account Id
     * @param withdrawAmount withdrawal amount
     * @return true if the withdrawal transaction went through successfully
     */
    public boolean doWithdraw(Bank bank, int accId, double withdrawAmount) {
        try {
            bank.retrieveAccount(accId).withdraw(withdrawAmount);
            System.out.println("Withdrawal complete.");
            return true;
        }catch(IllegalArgumentException e) {
            System.out.println("Funds not avaliable.");
            return false;
        }
    }

    /**
     *
     * @param bank bank
     * @param accId account ID
     * @return the balance left in the account
     */
    public double doBalance(Bank bank, int accId) {
        return bank.retrieveAccount(accId).getBalance();
    }
}