/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.bitforce;

import java.util.ArrayList;

/**
 * Used to retrieve an account from the account class.
 * @author Clayton
 */
public class Bank {

    private static ArrayList<Account> _accountList;

    /**
     * Instantiates an ArrayList with 10 account objects and assigns them a account ID, initial balance and random interest rate.
     */
    public Bank() {
        _accountList = new ArrayList<>();

        //Loop through the ArrayList and instantiate Accounts.
        for(int i = 0; i < 10; i++) {
            _accountList.add(new Account(i, 100));
            _accountList.get(i).setAnnualInterestRate(Math.random()*3+1);
        }
    }

    /**
     *
     * @param accId account ID
     * @return an Account
     */
    public Account retrieveAccount(int accId) {
            return _accountList.get(accId);
    }

    /**
     * Check if the account specified exists. If not, throws an IllegalArgumentException.
     * @param accId account ID
     */
    public void accountExists(int accId) {
        if(accId > _accountList.size()-1 || accId < 0) {
            throw new IllegalArgumentException("Account does not exist.");
        }
    }
}