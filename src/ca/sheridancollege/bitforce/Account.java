/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.bitforce;

import java.util.Date;

/**
 * Modifies and constructs new bank accounts
 * @author Clayton
 */
public class Account {
    private static int _id;
    private static double _balance;
    private static double _annualInterestRate;
    private static Date _dateCreated;

    /**
     * This is the default constructor, sets the id and balance to 0
     */
    public Account() {
        this(0, 100);
    }

    /**
     * Creates a new account and sets the ID and balance
     * @param id unique identifier of the account
     * @param balance initial balance in the account
     */
    public Account(int id, double balance) {
        _id = id;
        _balance = balance;
        _dateCreated = new Date(1356998400);
    }

    /**
     *
     * @return the id of the account
     */
    public int getId() {
        return _id;
    }

    /**
     * Sets the id of the account
     * @param id account ID
     */
    public void setId(int id) {
        _id = id;
    }

    /**
     *
     * @return the balance in the account
     */
    public double getBalance() {
        return _balance;
    }

    /**
     * Sets the balance in the account
     * @param balance account balance
     */
    public void setBalance(double balance) {
        _balance = balance;
    }

    /**
     *
     * @return the annual interest rate of the account
     */
    public double getAnnualInterestRate() {
        return (_annualInterestRate * 100);
    }

    /**
     * Sets the annual interest rate
     * @param annualInterestRate annual interest rate
     */
    public void setAnnualInterestRate(double annualInterestRate) {
        if(annualInterestRate > 0) {
            _annualInterestRate = (annualInterestRate / 100);
        }else {
            throw new IllegalArgumentException("Interest rate cannot be less than 0.");
        }
    }

    /**
     *
     * @return the date the account was created.
     */
    public Date getDateCreated() {
        return _dateCreated;
    }

    /**
     *
     * @return the monthly interest rate
     */
    public double getMonthlyInterestRate() {
        return (_annualInterestRate / 12) * 100;
    }

    /**
     *
     * @return the monthly interest amount
     */
    public double getMonthlyInterest() {
        return (getBalance() * getMonthlyInterestRate());
    }

    /**
     *
     * @param amount amount to withdraw
     * @return balance after withdrawal
     */
    public double withdraw(double amount) {
        if(amount > 0) {
            if(getBalance() > amount) {
                _balance -= amount;
            }else {
                throw new IllegalArgumentException("Withdrawal amount cannot be greater than the current balance.");
            }
        }else {
            throw new IllegalArgumentException("Withdrawal amount cannot be less than 0.");
        }
          return getBalance();
    }

    /**
     *
     * @param amount amount to deposit
     * @return balance after deposit
     */
    public double deposit(double amount) {
        if(amount > 0) {
            _balance += amount;
        }else {
            throw new IllegalArgumentException("Deposit amount cannot be less than 0.");
        }
        return getBalance();
    }
}